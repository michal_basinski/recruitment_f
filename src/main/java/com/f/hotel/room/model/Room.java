package com.f.hotel.room.model;

import com.f.hotel.room.dto.RoomDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "ROOMS")
class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long floor;
    private Long number;
    private Long capacity;
    private Double pricePerNight;

    @OneToMany(mappedBy = "room")
    private List<Reservation> reservations;

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    Long getFloor() {
        return floor;
    }

    void setFloor(Long floor) {
        this.floor = floor;
    }

    Long getNumber() {
        return number;
    }

    void setNumber(Long number) {
        this.number = number;
    }

    Long getCapacity() {
        return capacity;
    }

    void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    Double getPricePerNight() {
        return pricePerNight;
    }

    void setPricePerNight(Double pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    List<Reservation> getReservations() {
        return reservations;
    }

    void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    RoomDto toDto() {
        RoomDto roomDto = new RoomDto();
        roomDto.setFloor(this.floor);
        roomDto.setCapacity(this.capacity);
        roomDto.setNumber(this.number);
        roomDto.setPricePerNight(this.pricePerNight);
        return roomDto;
    }
}
