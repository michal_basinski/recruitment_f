package com.f.hotel.room.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class RoomConfiguration {

    @Bean
    RoomFacade roomFacade(RoomRepository roomRepository, ReservationRepository reservationRepository) {
        return new RoomFacade(roomRepository, reservationRepository);
    }

}
