package com.f.hotel.room.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

interface RoomRepository extends JpaRepository<Room, Long> {

    Optional<Room> findRoomByNumber(Long number);

}
