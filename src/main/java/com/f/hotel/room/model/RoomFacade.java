package com.f.hotel.room.model;

import com.f.hotel.room.dto.ReservationDto;
import com.f.hotel.room.dto.RoomDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RoomFacade {

    private static final String MESSAGE_UNAVAILABLE_ROOM = "Room is unavailable in given time period";

    private RoomRepository roomRepository;

    private ReservationRepository reservationRepository;

    RoomFacade(RoomRepository roomRepository, ReservationRepository reservationRepository) {
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
    }

    public Set<RoomDto> getAvailableRooms(LocalDate start, LocalDate end) {

        final Set<Long> unavailableRoomsIds = reservationRepository.getReservationsInTimePeriod(start, end).stream().map(x -> x.getRoom().getId()).collect(Collectors.toSet());

        //TODO: optionally rewrite using @Query in RoomRepository
        List<Room> availableRooms = roomRepository.findAll().stream().filter(x -> !unavailableRoomsIds.contains(x.getId())).collect(Collectors.toList());

        return availableRooms.stream().map(x -> x.toDto()).collect(Collectors.toSet());
    }

    public ReservationDto bookRoom(ReservationDto reservationDto) {
        Long roomNumber = reservationDto.getRoomNumber();
        LocalDate start = reservationDto.getStart();
        LocalDate end = reservationDto.getEnd();
        Room roomByNumber = roomRepository.findRoomByNumber(roomNumber).orElseThrow(RoomNotFoundException::new);

        ReservationDto result = new ReservationDto();
        Set<Long> availableRooms = getAvailableRooms(start, end).stream().map(x -> x.getNumber()).collect(Collectors.toSet());
        if (availableRooms.contains(roomByNumber.getNumber())) {
            Reservation reservation = new Reservation();
            reservation.setRoom(roomByNumber);
            reservation.setStart(start);
            reservation.setEnd(end);
            reservation.setGuestName(reservationDto.getGuestName());
            reservationRepository.save(reservation);
            result = reservation.toDto();
        } else {
            result.setMessage(MESSAGE_UNAVAILABLE_ROOM);
        }
        return result;
    }

    public void deleteBooking(Long reservationToDelete) {
        reservationRepository.deleteById(reservationToDelete);
    }
}
