package com.f.hotel.room.model;

import com.f.hotel.room.dto.ReservationDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

@Entity
@Table(name = "RESERVATIONS")
class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String guestName;

    private LocalDate start;

    private LocalDate end;

    @ManyToOne
    @JoinColumn(name = "ROOM_ID", nullable = false)
    private Room room;

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    String getGuestName() {
        return guestName;
    }

    void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    LocalDate getStart() {
        return start;
    }

    void setStart(LocalDate start) {
        this.start = start;
    }

    LocalDate getEnd() {
        return end;
    }

    void setEnd(LocalDate end) {
        this.end = end;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public ReservationDto toDto() {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setReservationId(this.id);
        reservationDto.setGuestName(this.getGuestName());
        reservationDto.setRoomNumber(this.getRoom().getNumber());
        reservationDto.setStart(this.start);
        reservationDto.setEnd(this.end);
        reservationDto.setPrice(this.room.getPricePerNight() * (1 + DAYS.between(start,end)));
        return reservationDto;
    }
}
