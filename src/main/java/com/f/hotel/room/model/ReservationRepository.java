package com.f.hotel.room.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query("from Reservation r where r.start BETWEEN :startDate AND :endDate OR r.end BETWEEN :startDate AND :endDate")
    List<Reservation> getReservationsInTimePeriod(@Param("startDate") LocalDate start, @Param("endDate") LocalDate end);

}
