package com.f.hotel.room;

import com.f.hotel.room.dto.ReservationDto;
import com.f.hotel.room.dto.RoomDto;
import com.f.hotel.room.model.RoomFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Set;

@RestController
class RoomController {

    private RoomFacade roomFacade;

    public RoomController(RoomFacade roomFacade) {
        this.roomFacade = roomFacade;
    }

    @GetMapping("/rooms/{start}/{end}")
    ResponseEntity getAvailableRooms(@PathVariable String start, @PathVariable String end) {

        Set<RoomDto> availableRooms = roomFacade.getAvailableRooms(LocalDate.parse(start), LocalDate.parse(end));
        return new ResponseEntity(availableRooms, HttpStatus.OK);
    }

    @PostMapping("/rooms")
    ResponseEntity bookRoom(@RequestBody ReservationDto reservationData) {
        ReservationDto reservationDto = roomFacade.bookRoom(reservationData);
        return new ResponseEntity(reservationDto, HttpStatus.OK);
    }

    @DeleteMapping("/rooms/{bookingId}")
    ResponseEntity deleteBooking(@PathVariable String bookingId) {
        roomFacade.deleteBooking(Long.valueOf(bookingId));
        return new ResponseEntity(HttpStatus.OK);
    }

}
