package com.f.hotel.room.model;

import com.f.hotel.room.dto.ReservationDto;
import com.f.hotel.room.dto.RoomDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(properties = {"spring.config.name=facade-test-h2","myapp.trx.datasource.url=jdbc:h2:mem:facadeTestDB"})
@DirtiesContext
public class RoomFacadeTest {

    @Autowired
    private RoomFacade roomFacade;

    @Test
    public void shouldReturnRooms101And103ForPeriodFrom01_03To06_03() {
        //given
        final LocalDate start = LocalDate.parse("2019-03-01");
        final LocalDate end = LocalDate.parse("2019-03-06");
        final Set<Long> expectedRooms = Arrays.asList(101l, 103l).stream().collect(Collectors.toSet());

        //when
        Set<RoomDto> availableRooms = roomFacade.getAvailableRooms(start, end);
        Set<Long> availableRoomNumbers = availableRooms.stream().map(x -> x.getNumber()).collect(Collectors.toSet());

        //then
        assertEquals(expectedRooms, availableRoomNumbers);
    }

    @Test
    public void shouldBookRoom101ForPeriodFrom01_03To06_03() {
        //given
        final Long roomNumber = 101l;
        final LocalDate start = LocalDate.parse("2019-03-01");
        final LocalDate end = LocalDate.parse("2019-03-06");
        final Double expectedPrice = 1800d;
        final ReservationDto request = new ReservationDto();
        request.setGuestName("Kowalski");
        request.setStart(start);
        request.setEnd(end);
        request.setRoomNumber(roomNumber);

        //when
        ReservationDto result = roomFacade.bookRoom(request);

        //then
        assertEquals(expectedPrice, result.getPrice());
    }

    @Test
    public void shouldNotBookRoom101ForPeriodFrom25_02To01_03() {
        //given
        final Long roomNumber = 101l;
        final LocalDate start = LocalDate.parse("2019-02-25");
        final LocalDate end = LocalDate.parse("2019-03-01");
        final String expectedMessage = "room is unavailable";
        final ReservationDto request = new ReservationDto();
        request.setGuestName("Kowalski");
        request.setStart(start);
        request.setEnd(end);
        request.setRoomNumber(roomNumber);

        //when
        ReservationDto result = roomFacade.bookRoom(request);

        //then
        assertTrue(result.getMessage().toLowerCase().contains(expectedMessage));
    }

    @Test
    public void shouldRemoveReservationForRoom102() {
        //given
        final LocalDate start = LocalDate.parse("2019-03-01");
        final LocalDate end = LocalDate.parse("2019-03-06");
        final Long expectedRoomNumber = 102l;
        final Long reservationNumberToDelete = 1l;

        Set<RoomDto> availableRooms = roomFacade.getAvailableRooms(start, end);
        Set<Long> availableRoomNumbers = availableRooms.stream().map(x -> x.getNumber()).collect(Collectors.toSet());

        assertTrue(!availableRoomNumbers.contains(expectedRoomNumber));

        //when
        roomFacade.deleteBooking(reservationNumberToDelete);

        availableRooms = roomFacade.getAvailableRooms(start, end);
        availableRoomNumbers = availableRooms.stream().map(x -> x.getNumber()).collect(Collectors.toSet());

        assertTrue(availableRoomNumbers.contains(expectedRoomNumber));

    }
}