package com.f.hotel.room.model;

import com.f.hotel.HotelApplication;
import com.f.hotel.room.dto.ReservationDto;
import com.f.hotel.room.dto.RoomDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = HotelApplication.class,
        properties = {"spring.config.name=full-test-h2", "myapp.trx.datasource.url=jdbc:h2:mem:fullScenarioDB"})
@DirtiesContext
public class FullScenario {

    @LocalServerPort
    private int localServerPort;

    @Autowired
    private TestRestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Test scenario
     * 1. Check rooms available in time period 03.03-06.03
     * 2. Make reservation for room no. 101
     * 3. Check rooms available in time period 03.03-06.03
     * 4. Delete reservation for room no. 101
     * 5. Check rooms available in time period 03.03-06.03
     */
    @Test
    public void showFullScenario() throws IOException {
        //given
        final String start = "2019-03-01";
        final String end = "2019-03-06";
        final Double expectedBookingCost = 1800d;
        final Set<Long> expectedAvailableRoomsInFirstAttempt = Arrays.asList(101l, 103l).stream().collect(Collectors.toSet());
        final Set<Long> expectedAvailableRoomsInSecondAttempt = Arrays.asList(103l).stream().collect(Collectors.toSet());
        final Set<Long> expectedAvailableRoomsInThirdAttempt = Arrays.asList(101l, 103l).stream().collect(Collectors.toSet());
        final CollectionType collectionTypeForJacksonDeserializer = objectMapper.getTypeFactory().constructCollectionType(Set.class, RoomDto.class);


        // 1. Check rooms available in time period 03.03-06.03
        ResponseEntity<String> availabilityResponse1 = restTemplate.getForEntity("http://localhost:" + localServerPort + "rooms/" + start + "/" + end, String.class);

        Set<RoomDto> availableRoomNumbersNo1 = objectMapper.readValue(availabilityResponse1.getBody(), collectionTypeForJacksonDeserializer);
        assertEquals(expectedAvailableRoomsInFirstAttempt, availableRoomNumbersNo1.stream().map(x -> x.getNumber()).collect(Collectors.toSet()));

        // 2. Book room no. 101 for time period 03.03-06.03
        ReservationDto request = new ReservationDto();
        request.setRoomNumber(101l);
        request.setStart(LocalDate.parse(start));
        request.setEnd(LocalDate.parse(end));
        request.setGuestName("Kowalski");
        ResponseEntity<ReservationDto> bookingResponse = restTemplate.postForEntity("http://localhost:" + localServerPort + "rooms", request, ReservationDto.class);
        assertEquals(expectedBookingCost, bookingResponse.getBody().getPrice());
        assertEquals(HttpStatus.OK, bookingResponse.getStatusCode());

        Long bookingNumber = bookingResponse.getBody().getReservationId();

        // 3. Check rooms available in time period 03.03-06.03
        ResponseEntity<String> availabilityResponse2 = restTemplate.getForEntity("http://localhost:" + localServerPort + "rooms/" + start + "/" + end, String.class);
        Set<RoomDto> availableRoomNumbersNo2 = objectMapper.readValue(availabilityResponse2.getBody(), collectionTypeForJacksonDeserializer);
        assertEquals(expectedAvailableRoomsInSecondAttempt, availableRoomNumbersNo2.stream().map(x -> x.getNumber()).collect(Collectors.toSet()));

        // 4. Delete booking
        restTemplate.delete("http://localhost:" + localServerPort + "/rooms/" + bookingNumber);

        // 5. Check rooms available in time period 03.03-06.03
        ResponseEntity<String> availabilityResponse3 = restTemplate.getForEntity("http://localhost:" + localServerPort + "rooms/" + start + "/" + end, String.class);
        Set<RoomDto> availableRoomNumbersNo3 = objectMapper.readValue(availabilityResponse3.getBody(), collectionTypeForJacksonDeserializer);
        assertEquals(expectedAvailableRoomsInThirdAttempt, availableRoomNumbersNo3.stream().map(x -> x.getNumber()).collect(Collectors.toSet()));
    }
}
